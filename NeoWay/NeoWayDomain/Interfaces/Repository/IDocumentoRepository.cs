﻿using System;
using System.Threading.Tasks;
using NeoWayDomain.Entities;
using NeoWayDomain.Filters;
using SE.EntityFrameworkCore.UnitOfWork;
using SE.EntityFrameworkCore.UnitOfWork.Collections;

namespace NeoWayDomain.Interfaces.Repository
{
    public interface IDocumentoRepository : IRepository<Documento>
    {
        Task<IPagedList<Documento>> BuscarTodosOsRegistros(DocumentoFilter documentoFilter);
    }
}
