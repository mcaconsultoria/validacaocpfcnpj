﻿using System;
using System.Threading.Tasks;
using NeoWayDomain.Entities;
using NeoWayDomain.Filters;
using SE.EntityFrameworkCore.UnitOfWork.Collections;

namespace NeoWayDomain.Interfaces.Services
{
    public interface IDocumentoService
    {
        Task<IPagedList<Documento>> BuscarTodosOsRegistrosAsync(DocumentoFilter documentoFilter);
        Task<Documento> ObterPorIdAsync(Guid id);
        Task<Documento> RemoverDocumentoAsync(Guid id);
        Task<Documento> InserirNovoDocumentoAsync(Documento documento);
        Task<Documento> AlterarDocumentoAsync(Guid id, Documento documento);
    }
}
