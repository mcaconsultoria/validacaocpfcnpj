﻿using System;
using System.Collections.Generic;

namespace NeoWayDomain.Interfaces
{
    public interface INotificationContext
    {
        public List<Dictionary<string, string>> Erros { get; }
        public bool ExisteNotificacao { get; }
    }
}
