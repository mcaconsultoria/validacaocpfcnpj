﻿namespace NeoWayDomain.Filters
{
    public class DocumentoFilter
    {
        /// <summary>
        /// Quantidade de regitros por página
        /// </summary>
        public int Limit { get; set; } = 10;
        /// <summary>
        /// Quantidade de registro na página
        /// </summary>
        public int Page { get; set; } = 0;
        /// <summary>
        /// Campo para ordenação
        /// </summary>
        /// <example>
        /// -CpfCnpj (Ordenação por documento ASC)
        /// CpfCnpj (Ordenação por documento DESC)
        /// </example>
        public string Sort { get; set; }

        /// <summary>
        /// Campo para filtro de busca
        /// </summary>
        public string CpfCnpj { get; set; }
        public bool ? Bloqueado { get; set; }
    }

}
