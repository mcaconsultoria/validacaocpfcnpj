﻿using System;
namespace NeoWayDomain.Entities
{
    public class Documento : BaseEntity
    {
        public string CpfCnpj { get; set; }
        public bool Bloqueado { get; set; } = false;
    }
}
