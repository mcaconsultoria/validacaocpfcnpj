﻿using System;
using Microsoft.AspNetCore.Http;
using Serilog.Events;

namespace NeoWayApplication
{
    public class SerilogApplicationBuilderExtensions
    {
        static LogEventLevel DefaultGetLevel(HttpContext ctx, double _, Exception ex) =>
         ex != null
             ? LogEventLevel.Error
             : ctx.Response.StatusCode > 499
                 ? LogEventLevel.Error
                 : LogEventLevel.Information;
    }
}
