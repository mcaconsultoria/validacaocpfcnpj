﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NeoWayApplication.Helper;
using NeoWayDomain.Interfaces;
using NeoWayDomain.Interfaces.Repository;
using NeoWayDomain.Interfaces.Services;
using NeoWayInfra.Data.Context;
using NeoWayInfra.Data.Repository;
using NeoWayService;
using NeoWayService.Services;
using Npgsql;
using SE.EntityFrameworkCore.UnitOfWork;
using Serilog;
using Serilog.Events;

namespace NeoWayApplication
{
    public class Startup
    {
        private readonly IWebHostEnvironment env;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            this.env = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            //Configuração Conexão com base e UnitOfWork
            if (!env.IsEnvironment("Test"))
            {
                services
                   .AddDbContext<SqlContext>(opt =>
                   {
                       opt.UseNpgsql(Configuration.GetConnectionString("neoway"), npgsqlOptions =>
                       {
                           npgsqlOptions.MigrationsAssembly("NeoWayInfra.Data");
                           npgsqlOptions.MigrationsHistoryTable("__EFMigrationsHistory");
                       });

#if DEBUG
                       opt.EnableSensitiveDataLogging();
#endif
                   })
                   .AddUnitOfWork<SqlContext>();
            } else
            {
                   services
                    .AddDbContext<SqlContext>(opt =>
                    {
                        opt.UseInMemoryDatabase("neoway");

#if DEBUG
                        opt.EnableSensitiveDataLogging();
#endif
                    })
                   .AddUnitOfWork<SqlContext>();
            }


            // Injeção de dependencias
            services.AddScoped<IDocumentoService, DocumentoService>();
            services.AddScoped<IDocumentoRepository, DocumentoRepository>();
            services.AddScoped<INotificationContext, NotificationContext>();

            //Carrega o swagger
            services.AddSwaggerGen(c =>
            {

                c.SwaggerDoc("v1",
                    new Microsoft.OpenApi.Models.OpenApiInfo
                    {
                        Title = "NeoWay - Validação Documento(CPF/CNPJ)",
                        Version = "v1",
                        Description = "Api Rest"
                    });

                var currentAssembly = Assembly.GetExecutingAssembly();
                var xmlDocs = currentAssembly.GetReferencedAssemblies()
                .Union(new AssemblyName[] { currentAssembly.GetName() })
                .Select(a => Path.Combine(Path.GetDirectoryName(currentAssembly.Location), $"{a.Name}.xml"))
                .Where(f => File.Exists(f)).ToArray();

                Array.ForEach(xmlDocs, (d) => {
                    c.IncludeXmlComments(d);
                });

                c.EnableAnnotations();
            });

            //Carrega automapper
            services.AddAutoMapper(typeof(Startup));

            //checkpoint
            services.AddHealthChecks();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            //Serilog
            app.UseSerilogRequestLogging(opts => {
                opts.EnrichDiagnosticContext = LogHelper.EnrichFromRequest;
                opts.GetLevel = LogHelper.GetLevel(LogEventLevel.Verbose, "Health checks");
            });

            if (env.IsDevelopment())
            {
                UpdateDataBase(app);
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/healthz"); //Add health check endpoint
                endpoints.MapControllers();

            });

            //Carrega o swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api NeoWay");
                c.DocumentTitle = "Validação Docuemnto (CPF/CNPJ)";
                c.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.List);
            });

        }

        private static void UpdateDataBase(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope();

            using var context = serviceScope.ServiceProvider.GetService<SqlContext>();
            context.Database.Migrate();

            using var conn = (NpgsqlConnection)context.Database.GetDbConnection();
            conn.Open();
            conn.ReloadTypes();
        }
    }
}
