﻿using System.Collections.Generic;
namespace NeoWayApplication.Response
{
    public class PagedListResponse<Entity> where Entity : class
    {
        /// <summary>
        /// asdfasd
        /// </summary>
        public int IndexFrom { get; set; }
        /// <summary>
        /// Página corrente
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// Quantidade de registro por página
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Quantidade total de registros
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// Existe página anterior?
        /// </summary>
        public bool HasPreviousPage { get; set; }
        /// <summary>
        /// Existe próxima página ?
        /// </summary>
        public bool hasNextPage { get; set; }
        /// <summary>
        /// Dados referente a tabela da base que está sendo consultada
        /// </summary>
        public List<Entity> Items { get; set; }

    }
}
