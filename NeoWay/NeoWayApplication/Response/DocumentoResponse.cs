﻿using System;
namespace NeoWayApplication.Response
{
    public class DocumentoResponse
    {

        /// <summary>
        /// Identificador do registro
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Cpf ou Cnpj que foi regitrado
        /// </summary>
        public string CpfCnpj { get; set; }
        /// <summary>
        /// Informa se o Cpf  ou Cnpj está bloqueado ou não
        /// </summary>
        public bool Bloqueado { get; set; }
    }
}
