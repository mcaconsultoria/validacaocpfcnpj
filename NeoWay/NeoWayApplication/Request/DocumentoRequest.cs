﻿using System;
namespace NeoWayApplication.Request
{
    public class DocumentoRequest
    {
        /// <summary>
        /// Cpf ou Cnpj
        /// </summary>
        public string CpfCnpj { get; set; }
        /// <summary>
        /// Informa se o Cpf  ou Cnpj está bloqueado ou não
        /// </summary>
        public bool Bloqueado { get; set; } = false;
    }
}
