#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

WORKDIR /src
COPY ["NeoWayApplication/NeoWayApplication.csproj", "NeoWayApplication/"]
COPY ["NeoWayDomain/NeoWayDomain.csproj", "NeoWayDomain/"]
COPY ["NeoWayService/NeoWayService.csproj", "NeoWayService/"]
COPY ["NeoWayInfra.Data/NeoWayInfra.Data.csproj", "NeoWayInfra.Data/"]
COPY ["tests/NeoWayIntegationTest/NeoWayIntegationTest.csproj", "tests/NeoWayIntegationTest/"]
COPY ["docker-compose.dcproj", "docker-compose.dcproj"]
COPY ["NeoWay.sln", "NeoWay.sln"]

RUN dotnet restore "NeoWay.sln"
COPY . .
WORKDIR "NeoWayApplication"
RUN dotnet build "NeoWayApplication.csproj" -c Debug -o /app/build

FROM build AS publish
RUN dotnet publish "NeoWayApplication.csproj" -c Debug -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "NeoWayApplication.dll"]
