﻿using System;
using AutoMapper;
using NeoWayApplication.Request;
using NeoWayApplication.Response;
using NeoWayDomain.Entities;
using SE.EntityFrameworkCore.UnitOfWork.Collections;

namespace NeoWayApplication.Mapper
{
    public class DocumentoMapper : Profile
    {
        public DocumentoMapper()
        {
            CreateMap<IPagedList<Documento>, PagedListResponse<DocumentoResponse>>();
            CreateMap<Documento, DocumentoResponse>();
            CreateMap<DocumentoRequest, Documento>();
        }
    }
}
