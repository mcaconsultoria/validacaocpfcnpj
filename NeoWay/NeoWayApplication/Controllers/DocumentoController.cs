﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NeoWayApplication.Request;
using NeoWayApplication.Response;
using NeoWayDomain.Entities;
using NeoWayDomain.Filters;
using NeoWayDomain.Interfaces;
using NeoWayDomain.Interfaces.Services;
using NeoWayService.Validators;
using Serilog;

namespace NeoWayApplication.Controllers
{
    [ApiController]
    [Route("documento")]
    public class DocumentoController : ControllerBase
    {
        private readonly IDocumentoService documentoService;
        private readonly IMapper imapper;
        private readonly INotificationContext notificationContext;

        public DocumentoController(IDocumentoService documentoService, IMapper imapper, INotificationContext notificationContext)
        {
            this.documentoService = documentoService;
            this.imapper = imapper;
            this.notificationContext = notificationContext;
            
        }

        [HttpPost]
        public async Task<ActionResult<DocumentoResponse>> Post(DocumentoRequest documentoRequest)
        {
            var result = await documentoService.InserirNovoDocumentoAsync(imapper.Map<Documento>(documentoRequest));
            if(this.notificationContext.ExisteNotificacao)
            {
                return BadRequest(this.notificationContext.Erros);
            }
            return Created(nameof(Get), new { id = result .Id});
        }

        [HttpGet]
        public async Task<ActionResult<PagedListResponse<DocumentoResponse>>> Get([FromQuery] DocumentoFilter documentoFilter)
        {   
            var result = await documentoService.BuscarTodosOsRegistrosAsync(documentoFilter);
            return Ok(imapper.Map<PagedListResponse<DocumentoResponse>>(result));
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<DocumentoResponse>> GetById(Guid id)
        {
            var result = await documentoService.ObterPorIdAsync(id);
            if (this.notificationContext.ExisteNotificacao)
            {
                return BadRequest(this.notificationContext.Erros);
            }
            return Ok(imapper.Map<DocumentoResponse>(result));
        }


        [HttpPut("{id:guid}")]
        public async Task<ActionResult<DocumentoResponse>> Put(Guid id, [FromBody] DocumentoRequest documentoRequest)
        {
            Documento documento = imapper.Map<Documento>(documentoRequest);
            var result = await documentoService.AlterarDocumentoAsync(id, documento);

            if (this.notificationContext.ExisteNotificacao)
            {
                return BadRequest(this.notificationContext.Erros);
            }
            return Ok(imapper.Map<DocumentoResponse>(result));
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<DocumentoResponse>> Delete(Guid id)
        {
            var result = await documentoService.RemoverDocumentoAsync(id);
            if (this.notificationContext.ExisteNotificacao)
            {
                return BadRequest(this.notificationContext.Erros);
            }
            return Ok(imapper.Map<DocumentoResponse>(result));
        }

    }
}
