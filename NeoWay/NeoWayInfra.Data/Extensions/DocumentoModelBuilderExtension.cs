﻿using System;
using Microsoft.EntityFrameworkCore;
using NeoWayDomain.Entities;

namespace NeoWayInfra.Data.Extensions
{
    public static class DocumentoModelBuilderExtension
    {
        public static void SeedDocumentos(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Documento>().HasData(
                new Documento() { CpfCnpj = "08236053000118", Bloqueado = true },
                new Documento() { CpfCnpj = "87246340000185", Bloqueado = true },
                new Documento() { CpfCnpj = "87529319000197", Bloqueado = true },
                new Documento() { CpfCnpj = "03699939000184", Bloqueado = true },
                new Documento() { CpfCnpj = "13586798000147", Bloqueado = true },
                new Documento() { CpfCnpj = "51632531000138" },
                new Documento() { CpfCnpj = "69913191000189" },
                new Documento() { CpfCnpj = "97686399000148" },
                new Documento() { CpfCnpj = "46615404000190" },
                new Documento() { CpfCnpj = "77296070000163" },

                new Documento() { CpfCnpj = "58161251096", Bloqueado = true },         
                new Documento() { CpfCnpj = "74989610083", Bloqueado = true },
                new Documento() { CpfCnpj = "15711942077", Bloqueado = true },
                new Documento() { CpfCnpj = "74920639031", Bloqueado = true },
                new Documento() { CpfCnpj = "04787644050", Bloqueado = true },
                new Documento() { CpfCnpj = "11374202002" },
                new Documento() { CpfCnpj = "08366830055" },
                new Documento() { CpfCnpj = "96647899090" },
                new Documento() { CpfCnpj = "54854208007" },
                new Documento() { CpfCnpj = "46963911088" }

                );
        }
    }
}
