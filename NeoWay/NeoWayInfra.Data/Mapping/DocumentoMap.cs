﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NeoWayDomain.Entities;

namespace NeoWayInfra.Data.Mapping
{
    public class DocumentoMap : IEntityTypeConfiguration<Documento>
    {
       
        public void Configure(EntityTypeBuilder<Documento> builder)
        {
            builder.ToTable("Documento");

            builder.HasKey(prop => prop.Id);

            builder.Property(prop => prop.CpfCnpj)
                .HasConversion(prop => prop.ToString(), prop => prop)
                .IsRequired()
                .HasColumnName("CpfCnpj")
                .HasColumnType("varchar(14)");

            builder.Property(prop => prop.Bloqueado)
                .HasColumnName("Bloqueado");
        }
    }
}
