﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NeoWayInfra.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Documento",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    CpfCnpj = table.Column<string>(type: "varchar(14)", nullable: false),
                    Bloqueado = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documento", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Documento",
                columns: new[] { "Id", "Bloqueado", "CpfCnpj" },
                values: new object[,]
                {
                    { new Guid("1caa2ddc-1aa0-44f6-a058-ea7d0801f4bf"), true, "08236053000118" },
                    { new Guid("5599ec42-0de5-475e-9fe6-b5af8903e768"), false, "96647899090" },
                    { new Guid("7559571c-ec27-4fc4-97ca-c09838133bd8"), false, "08366830055" },
                    { new Guid("3c85cd8d-3853-432c-be34-fdfe6b4c8b38"), false, "11374202002" },
                    { new Guid("f20cb68c-ae02-42e5-8623-68b3c343be11"), true, "04787644050" },
                    { new Guid("cc1660f7-676e-4955-aec8-7cd29ab51678"), true, "74920639031" },
                    { new Guid("90a9a2d1-66e5-417b-92af-0cda8debbdbb"), true, "15711942077" },
                    { new Guid("ac5222af-3039-41c6-98d2-9c1757fd60ac"), true, "74989610083" },
                    { new Guid("a8045ccd-48fb-4d8c-b376-af35dcf9d146"), true, "58161251096" },
                    { new Guid("27cca593-74f2-4ca1-a7b7-fcba82d3acb4"), false, "77296070000163" },
                    { new Guid("6d34350d-f326-439e-9281-6134bb272df8"), false, "46615404000190" },
                    { new Guid("8dfaaa6e-e3b9-40b9-9c63-b7e60cbfaabb"), false, "97686399000148" },
                    { new Guid("eb840f7a-49fc-4776-b4f6-00ead90772d8"), false, "69913191000189" },
                    { new Guid("e099635f-0691-4ed5-84f2-1207ac300c63"), false, "51632531000138" },
                    { new Guid("7a5b3e25-7e1e-43b5-9558-706d39684cae"), true, "13586798000147" },
                    { new Guid("482ad2f3-7914-430e-bfdc-826aad04c757"), true, "03699939000184" },
                    { new Guid("042fcfbc-0bd2-46d7-aed9-6c6303acb213"), true, "87529319000197" },
                    { new Guid("ed5f75c2-d4db-4e25-b9c8-80591168bda6"), true, "87246340000185" },
                    { new Guid("b881d31c-e75b-4e9d-b9de-b3946468a35c"), false, "54854208007" },
                    { new Guid("bcacbba0-2a43-408e-b86d-73fa10fb917a"), false, "46963911088" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Documento");
        }
    }
}
