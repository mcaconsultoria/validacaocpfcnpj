﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using NeoWayDomain.Entities;
using NeoWayDomain.Filters;
using NeoWayDomain.Interfaces.Repository;
using NeoWayInfra.Data.Context;
using SE.EntityFrameworkCore.UnitOfWork;
using SE.EntityFrameworkCore.UnitOfWork.Collections;

namespace NeoWayInfra.Data.Repository
{
    public class DocumentoRepository : Repository<Documento>, IDocumentoRepository
    {
        public DocumentoRepository(SqlContext sqlContext) : base(sqlContext)
        {
        }

        public async Task<IPagedList<Documento>> BuscarTodosOsRegistros(DocumentoFilter documentoFilter)
        {
            Expression<Func<Documento, object>> sortExpression = (x => x.CpfCnpj);
            if (!string.IsNullOrEmpty(documentoFilter.Sort))
            {
                sortExpression = documentoFilter.Sort.Replace("-", "") switch
                {
                    "CpfCnpj" => (x => x.CpfCnpj),
                    "Bloqueado" => (x => x.Bloqueado)
                };
            }
            return await GetPagedListAsync(
                predicate: p => ((string.IsNullOrEmpty(documentoFilter.CpfCnpj)) ? true : p.CpfCnpj.Contains(documentoFilter.CpfCnpj)) && ((documentoFilter.Bloqueado != null)?p.Bloqueado == documentoFilter.Bloqueado:true),
                pageSize: documentoFilter.Limit,
                pageIndex: documentoFilter.Page,
                orderBy: o => documentoFilter.Sort?.StartsWith("-") ?? false ? o.OrderByDescending(sortExpression) : o.OrderBy(sortExpression)
                );
        }
    }
}
