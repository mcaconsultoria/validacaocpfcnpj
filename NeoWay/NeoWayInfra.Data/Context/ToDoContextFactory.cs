﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace NeoWayInfra.Data.Context
{
    public class ToDoContextFactory : IDesignTimeDbContextFactory<SqlContext>
    {
        public SqlContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<SqlContext>();
            builder.UseNpgsql("Host=postgre;Database=neoway;Username=postgres;Password=postgres");
            return new SqlContext(builder.Options);
        }
    }
}
