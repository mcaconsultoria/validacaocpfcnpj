﻿using Microsoft.EntityFrameworkCore;
using NeoWayDomain.Entities;
using NeoWayInfra.Data.Extensions;
using NeoWayInfra.Data.Mapping;

namespace NeoWayInfra.Data.Context
{
    public class SqlContext : DbContext
    {
        public SqlContext(DbContextOptions<SqlContext> options) : base(options)
        {
        }

        public SqlContext()
        {

        }
        public DbSet<Documento> Documentos { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Documento>(new DocumentoMap().Configure);
            modelBuilder.SeedDocumentos();
        }
    }
}
