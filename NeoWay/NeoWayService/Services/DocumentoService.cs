﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NeoWayDomain.Entities;
using NeoWayDomain.Filters;
using NeoWayDomain.Interfaces;
using NeoWayDomain.Interfaces.Repository;
using NeoWayDomain.Interfaces.Services;
using NeoWayService.Validators;
using SE.EntityFrameworkCore.UnitOfWork;
using SE.EntityFrameworkCore.UnitOfWork.Collections;

namespace NeoWayService.Services
{
    public class DocumentoService : IDocumentoService
    {
        private readonly IDocumentoRepository documentoRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly INotificationContext notificationContext;

        public DocumentoService(IDocumentoRepository documentoRepository, IUnitOfWork unitOfWork, INotificationContext notificationContext)
        {
            this.documentoRepository = documentoRepository;
            this.unitOfWork = unitOfWork;
            this.notificationContext = notificationContext;
        }

        public async Task<Documento> AlterarDocumentoAsync(Guid id, Documento documento)
        {

            var documentoBase = await documentoRepository.FindAsync(id);

            if (documentoBase is not null)
            {
                var validator = new DocumentoValidator();
                var validate = await validator.ValidateAsync(documento);
                if (validate.IsValid)
                {
                    documentoBase.Bloqueado = documento.Bloqueado;
                    documentoBase.CpfCnpj = documento.CpfCnpj;
                    documentoRepository.Update(documentoBase);
                    await unitOfWork.SaveChangesAsync();
                    return documentoBase;
                }

                foreach (var erro in validate.Errors)
                {
                    Dictionary<string, string> dicValidacao = new Dictionary<string, string>();
                    dicValidacao.Add(erro.PropertyName, erro.ErrorMessage);
                    this.notificationContext.Erros.Add(dicValidacao);
                }

                return default;
            }


            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add(id.ToString(), "O documento que você deseja alterar não existe em nossa base de dados");
            this.notificationContext.Erros.Add(dic);
            return default;
        }

        public async Task<IPagedList<Documento>> BuscarTodosOsRegistrosAsync(DocumentoFilter documentoFilter)
        {
            return await documentoRepository.BuscarTodosOsRegistros(documentoFilter);
        }

        public async Task<Documento> InserirNovoDocumentoAsync(Documento documento)
        {
            var validator = new DocumentoValidator();
            var validate = await validator.ValidateAsync(documento);

            if (validate.IsValid)
            {
                await documentoRepository.InsertAsync(documento);
                await unitOfWork.SaveChangesAsync();
            }

            foreach(var erro in validate.Errors)
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add(erro.PropertyName, erro.ErrorMessage);
                this.notificationContext.Erros.Add(dic);
            }

            return documento;
        }

        public async Task<Documento> ObterPorIdAsync(Guid id)
        {
            var documentoBase = await documentoRepository.FindAsync(id);
            if (documentoBase is not null)
            {
                return documentoBase;
            }
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add(id.ToString(), "O documento que você deseja buscar não existe em nossa base de dados");
            this.notificationContext.Erros.Add(dic);
            return default;
        }

        public async Task<Documento> RemoverDocumentoAsync(Guid id)
        {
            var documentoBase = await documentoRepository.FindAsync(id);

            if (documentoBase is not null)
            {
                documentoRepository.Delete(documentoBase);
                await unitOfWork.SaveChangesAsync();
                return documentoBase;
            }
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add(id.ToString(), "O documento que você deseja excluir não existe em nossa base de dados");
            this.notificationContext.Erros.Add(dic);
            return default;
            
        }

    }
}
