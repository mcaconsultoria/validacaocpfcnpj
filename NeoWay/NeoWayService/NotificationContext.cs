﻿using System;
using System.Collections.Generic;
using NeoWayDomain.Interfaces;

namespace NeoWayService
{
    public class NotificationContext : INotificationContext
    {


        public NotificationContext()
        {
            Erros = new List<Dictionary<string, string>>();
        }
        public List<Dictionary<string, string>> Erros { get; }
        public bool ExisteNotificacao {
            get {
                return Erros.Count > 0;
            }
        }

    }
}
