﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using NeoWayDomain.Entities;

namespace NeoWayService.Validators
{
    public class DocumentoValidator : AbstractValidator<Documento>
    {
        public DocumentoValidator()
        {
            RuleFor(x => x.CpfCnpj).NotEmpty()
                .MinimumLength(11)
                .MaximumLength(14)
                .Must(BeAValidDigits).WithMessage("Documento inválido");
        }


        private bool BeAValidDigits(string value)
        {
            if (
                string.IsNullOrEmpty(value) || 
                (value.Length == 11 && (value == "00000000000" || value == "11111111111" ||
                value == "22222222222" || value == "33333333333" ||
                value == "44444444444" || value == "55555555555" ||
                value == "66666666666" || value == "77777777777" ||
                value == "88888888888" || value == "99999999999")
                ) ||
                (value.Length == 14 && (value == "00000000000000" || value == "11111111111111" ||
                value == "22222222222222" || value == "33333333333333" ||
                value == "44444444444444" || value == "55555555555555" ||
                value == "66666666666666" || value == "77777777777777" ||
                value == "88888888888888" || value == "99999999999999")
                )

                )
            {
                return false;
            }
            
            var digito1 = Calculo(value);
            var digito2 = Calculo(value, 1);
            var digito = value.ToString().Substring(value.ToString().Length - 2);

            var r = (bool)(digito == $"{digito1}{digito2}");
            return r;
        }


        private int Calculo(string documento, int length = 2)
        {
            
            var raiz = documento.ToString().Substring(0, documento.ToString().Length - length).Reverse().ToArray();
            
            List<float> calculo = new List<float>();
            var i = 2;
            foreach (var x in raiz)
            {
                calculo.Add(Convert.ToInt32(x.ToString()) * i);
                i++;
                if(i > 9 && documento.Length == 14)
                {
                    i = 2;
                }
            }

            int soma = Convert.ToInt32(calculo.Sum());
            int resto = soma % 11;
            int digito1 = 0;
            if (resto >= 2)
            {
                digito1 = 11 - resto;
            }

            return digito1;
        }
    }
}
