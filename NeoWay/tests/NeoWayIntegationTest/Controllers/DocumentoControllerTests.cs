﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using NeoWayApplication.Request;
using NeoWayApplication.Response;
using NeoWayDomain.Entities;
using NeoWayDomain.Filters;
using NeoWayInfra.Data.Context;
using NeoWayIntegationTest;
using Xunit;

namespace NeoWayIntegationTests.Controllers
{

    public class DocumentoControllerTests : CustomWebApplicationFactory
    {
        public HttpClient Client { get; private set; }
        public SqlContext SqlContext { get; private set; }

        public DocumentoControllerTests()
        {
            this.Client = base.CreateClient();
            this.SqlContext = base.Services.GetRequiredService<SqlContext>();
        }

        private async Task LimparBase()
        {
            var todosOsRegistros = this.SqlContext.Documentos.ToList();
            this.SqlContext.Documentos.RemoveRange(todosOsRegistros);
            await this.SqlContext.SaveChangesAsync();
        }

        [Theory]
        [InlineData("11222333000181",false, 201)]
        [InlineData("84995455000184", true, 201)]
        [InlineData("75367392000120", false, 201)]
        [InlineData("19450651000101", true, 201)]
        [InlineData("07087212000105", false, 201)]
        [InlineData("96490095076", false, 201)]
        [InlineData("47262613049", false, 201)]
        [InlineData("45598022036", true, 201)]
        [InlineData("04312367043", false, 201)]
        [InlineData("13740557001", true, 201)]

        [InlineData("11222333000180", false, 400)]
        [InlineData("84995455000180", false, 400)]
        [InlineData("75367392000121", false, 400)]
        [InlineData("19450651000100", false, 400)]
        [InlineData("07087212000106", false, 400)]
        [InlineData("96490095071", false, 400)]
        [InlineData("47262613042", false, 400)]
        [InlineData("45598022011", false, 400)]
        [InlineData("04312367013", false, 400)]
        [InlineData("13740557011", false, 400)]

        [InlineData("00000000000", false, 400)]
        [InlineData("00000000000000", false, 400)]

        [InlineData("11111111111", false, 400)]
        [InlineData("11111111111111", false, 400)]
        public async Task PostAsyncDeveCriarODocumentoJuntoABaseDeDados(string documento, bool bloqueado, int statusCode)
        {
            await LimparBase();
            using var requestMessage = new HttpRequestMessage(HttpMethod.Post, "documento");
            DocumentoRequest documentoRequest = new DocumentoRequest()
            {
                CpfCnpj = documento,
                Bloqueado = bloqueado
            };
            requestMessage.Content = new StringContent(JsonSerializer.Serialize(documentoRequest), Encoding.UTF8, "application/json");

            ////act
            var response = await Client.SendAsync(requestMessage);
            ((int)response.StatusCode).Should().Be(statusCode);

            var responseString = await response.Content.ReadAsStringAsync();
            if (statusCode == 201)
            {
                var documentoResponse = JsonSerializer.Deserialize<DocumentoIdResponse>(responseString, Utils.JsonSerializerOption);

                var documentoBase = await this.SqlContext.Documentos.FindAsync(documentoResponse.Id);

                documentoBase.CpfCnpj.Should().Be(documento);
                documentoBase.Bloqueado.Should().Be(bloqueado);
            } else
            {
                responseString.Should().Contain("[{\"CpfCnpj\":\"Documento inválido\"}]");
            }

            
        }


        [Theory]
        [InlineData("11222333000181", null, 1)]
        [InlineData("11222333000181", true, 0)]
        [InlineData("001", null, 6)]
        [InlineData("", true, 2)]
        [InlineData("", false, 8)]
        [InlineData("", null, 10)]
        public async Task GetAsyncDeveListarOsDocumentoJuntoABaseDeDados(string filtroDocumento, bool? bloqueado, int total)
        {
            await LimparBase();
            if (this.SqlContext.Documentos.Count() == 0)
            {
                List<Documento> documentos = new List<Documento>()
            {
                { new Documento() { CpfCnpj = "11222333000181",Bloqueado = false }},
                { new Documento() { CpfCnpj = "84995455000184",Bloqueado = true }},
                { new Documento() { CpfCnpj = "75367392000120",Bloqueado = false }},
                { new Documento() { CpfCnpj = "19450651000101",Bloqueado = false }},
                { new Documento() { CpfCnpj = "07087212000105",Bloqueado = false }},
                { new Documento() { CpfCnpj = "96490095076",Bloqueado = false }},
                { new Documento() { CpfCnpj = "47262613049",Bloqueado = true }},
                { new Documento() { CpfCnpj = "45598022036",Bloqueado = false }},
                { new Documento() { CpfCnpj = "04312367043",Bloqueado = false }},
                { new Documento() { CpfCnpj = "13740557001",Bloqueado = false }}

            };
                await this.SqlContext.AddRangeAsync(documentos);
                await this.SqlContext.SaveChangesAsync();
            }

            using var requestMessage = new HttpRequestMessage(HttpMethod.Get, $"documento?CpfCnpj={filtroDocumento}&bloqueado={bloqueado}");
            requestMessage.Content = new StringContent("", Encoding.UTF8, "application/json");

            ////act
            var response = await Client.SendAsync(requestMessage);
            ((int)response.StatusCode).Should().Be(200);
            var responseString = await response.Content.ReadAsStringAsync();
            var retorno = JsonSerializer.Deserialize<PagedListResponse<DocumentoResponse>>(responseString, Utils.JsonSerializerOption);

            retorno.Items.Should().HaveCount(total);

        }


        [Fact]
        public async Task GetByIdAsyncDeveListarOsDocumentoJuntoABaseDeDados()
        {
            var documento = new Documento() { CpfCnpj = "11222333000181", Bloqueado = false };
            await this.SqlContext.AddRangeAsync(documento);
            await this.SqlContext.SaveChangesAsync();


            using var requestMessage = new HttpRequestMessage(HttpMethod.Get, $"documento/{documento.Id}");
            requestMessage.Content = new StringContent("", Encoding.UTF8, "application/json");

            ////act
            var response = await Client.SendAsync(requestMessage);
            ((int)response.StatusCode).Should().Be(200);
            var responseString = await response.Content.ReadAsStringAsync();
            var retorno = JsonSerializer.Deserialize<DocumentoResponse>(responseString, Utils.JsonSerializerOption);

            retorno.CpfCnpj.Should().Be(documento.CpfCnpj);

        }

        [Fact]
        public async Task GetByIdInvalidoAsyncDeveApresentarErro()
        {

            using var requestMessage = new HttpRequestMessage(HttpMethod.Get, "documento/bfd21d01-f56d-483b-a147-dd54d35d15d7");
            requestMessage.Content = new StringContent("", Encoding.UTF8, "application/json");

            ////act
            var response = await Client.SendAsync(requestMessage);
            ((int)response.StatusCode).Should().Be(400);

            var responseString = await response.Content.ReadAsStringAsync();
            responseString.Should().Be("[{\"bfd21d01-f56d-483b-a147-dd54d35d15d7\":\"O documento que você deseja buscar não existe em nossa base de dados\"}]");

        }

        [Fact]
        public async Task DelByIdAsyncDeveApresentarORegistroExcluido()
        {
            var documento = new Documento() { CpfCnpj = "11222333000181", Bloqueado = false };
            await this.SqlContext.AddRangeAsync(documento);
            await this.SqlContext.SaveChangesAsync();

            using var requestMessage = new HttpRequestMessage(HttpMethod.Delete, $"documento/{documento.Id}");
            requestMessage.Content = new StringContent("", Encoding.UTF8, "application/json");

            ////act
            var response = await Client.SendAsync(requestMessage);
            ((int)response.StatusCode).Should().Be(200);

            var responseString = await response.Content.ReadAsStringAsync();
            var retorno = JsonSerializer.Deserialize<DocumentoResponse>(responseString, Utils.JsonSerializerOption);
            retorno.CpfCnpj.Should().Be(documento.CpfCnpj);

        }

        [Fact]
        public async Task DelByIdInvalidoAsyncDeveApresentarErro()
        {

            using var requestMessage = new HttpRequestMessage(HttpMethod.Delete, "documento/bfd21d01-f56d-483b-a147-dd54d35d15d7");
            requestMessage.Content = new StringContent("", Encoding.UTF8, "application/json");

            ////act
            var response = await Client.SendAsync(requestMessage);
            ((int)response.StatusCode).Should().Be(400);

            var responseString = await response.Content.ReadAsStringAsync();
            responseString.Should().Be("[{\"bfd21d01-f56d-483b-a147-dd54d35d15d7\":\"O documento que você deseja excluir não existe em nossa base de dados\"}]");

        }

        [Fact]
        public async Task PutInvalidoAsyncDeveApresentarErro()
        {
            DocumentoRequest documentoRequest = new DocumentoRequest()
            {
                CpfCnpj = "123123"
            };
            using var requestMessage = new HttpRequestMessage(HttpMethod.Put, "documento/bfd21d01-f56d-483b-a147-dd54d35d15d7");
            requestMessage.Content = new StringContent(JsonSerializer.Serialize(documentoRequest), Encoding.UTF8, "application/json");


            ////act
            var response = await Client.SendAsync(requestMessage);
            ((int)response.StatusCode).Should().Be(400);

            var responseString = await response.Content.ReadAsStringAsync();
            responseString.Should().Be("[{\"bfd21d01-f56d-483b-a147-dd54d35d15d7\":\"O documento que você deseja alterar não existe em nossa base de dados\"}]");

        }

        [Fact]
        public async Task PutAsyncDeveApresentarORegistroAlteardo()
        {
            await LimparBase();
            var documento = new Documento() { CpfCnpj = "11222333000181", Bloqueado = false };
            await this.SqlContext.AddRangeAsync(documento);
            await this.SqlContext.SaveChangesAsync();

            DocumentoRequest documentoRequest = new DocumentoRequest()
            {
                CpfCnpj = documento.CpfCnpj,
                Bloqueado = true
            };
            using var requestMessage = new HttpRequestMessage(HttpMethod.Put, $"documento/{documento.Id}");
            requestMessage.Content = new StringContent(JsonSerializer.Serialize(documentoRequest), Encoding.UTF8, "application/json");


            ////act
            var response = await Client.SendAsync(requestMessage);
            ((int)response.StatusCode).Should().Be(200);

            var responseString = await response.Content.ReadAsStringAsync();
            var retorno = JsonSerializer.Deserialize<DocumentoResponse>(responseString, Utils.JsonSerializerOption);
            retorno.CpfCnpj.Should().Be(documento.CpfCnpj);
            retorno.Bloqueado.Should().Be(true);
        }

        [Fact]
        public async Task PutAsyncDeveApresentarErroDeDocumentoInvalido()
        {
            await LimparBase();
            var documento = new Documento() { CpfCnpj = "11222333000181", Bloqueado = false };
            await this.SqlContext.AddRangeAsync(documento);
            await this.SqlContext.SaveChangesAsync();

            DocumentoRequest documentoRequest = new DocumentoRequest()
            {
                CpfCnpj = "11111111111",
                Bloqueado = true
            };
            using var requestMessage = new HttpRequestMessage(HttpMethod.Put, $"documento/{documento.Id}");
            requestMessage.Content = new StringContent(JsonSerializer.Serialize(documentoRequest), Encoding.UTF8, "application/json");


            ////act
            var response = await Client.SendAsync(requestMessage);
            ((int)response.StatusCode).Should().Be(400);

            var responseString = await response.Content.ReadAsStringAsync();
            responseString.Should().Contain("[{\"CpfCnpj\":\"Documento inválido\"}]");
        }
    }
}
