﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace NeoWayIntegationTest
{
    public static class Utils
    {
        public static JsonSerializerOptions JsonSerializerOption {

            get
            {
                var serializeOptions = new JsonSerializerOptions()
                {
                    DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull,
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    NumberHandling = System.Text.Json.Serialization.JsonNumberHandling.Strict,
                    AllowTrailingCommas = true
                };

                serializeOptions.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase, true));
                return serializeOptions;
            }
        }
    }
}
