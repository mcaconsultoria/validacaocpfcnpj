﻿using System;
using DotNet.Testcontainers.Containers.Builders;
using DotNet.Testcontainers.Containers.Configurations.Databases;
using DotNet.Testcontainers.Containers.Modules.Databases;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using NeoWayApplication;

namespace NeoWayIntegationTests
{
    /// <summary>
    /// Cria container para base de dados
    /// </summary>
    public class CustomWebApplicationFactory : WebApplicationFactory<Startup>
    {

        
        public CustomWebApplicationFactory()
        {
           
        }


        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.UseEnvironment("Test");
        }

        
    }
}
