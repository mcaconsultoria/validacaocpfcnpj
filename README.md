# README #

Este projeto valida o digito verificador de um CPF / CNPJ

### Como rodar o projeto? ###

* Pré requisito docker
* Acessar a past do projeto pelo terminal e executar o seguinte comando:
* docker compose up

### Links de Acesso ###

* http://localhost:57012/swagger/index.html
* http://localhost:8080